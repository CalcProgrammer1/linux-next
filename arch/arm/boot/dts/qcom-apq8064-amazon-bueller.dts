// SPDX-License-Identifier: GPL-2.0
#include "qcom-apq8064-v2.0.dtsi"
#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/pinctrl/qcom,pmic-gpio.h>

/ {
	model = "Amazon Fire TV";
	compatible = "amazon,bueller", "qcom,apq8064";

	aliases {
		serial0 = &gsbi4_serial;
	};

	chosen {
		stdout-path = "serial0:115200n8";
	};

        hdmi-out {
                compatible = "hdmi-connector";
                type = "a";

                port {
                        hdmi0_con: endpoint {
                                remote-endpoint = <&hdmi0_out>;
                        };
                };
        };

	soc {
		rpm@108000 {
			regulators {
				vin_lvs1_3_6-supply = <&pm8921_s4>;
				vin_lvs2-supply = <&pm8921_s1>;
				vin_lvs4_5_7-supply = <&pm8921_s4>;

				vdd_l1_l2_l12_l18-supply = <&pm8921_s4>;
				vdd_l24-supply = <&pm8921_s1>;
				vdd_l25-supply = <&pm8921_s1>;
				vdd_l26-supply = <&pm8921_s7>;
				vdd_l27-supply = <&pm8921_s7>;
				vdd_l28-supply = <&pm8921_s7>;


				/* Buck SMPS */
				s1 {
					regulator-always-on;
					regulator-min-microvolt = <1225000>;
					regulator-max-microvolt = <1225000>;
					qcom,switch-mode-frequency = <3200000>;
					bias-pull-down;
				};

				s2 {
					regulator-min-microvolt = <1300000>;
					regulator-max-microvolt = <1300000>;
					qcom,switch-mode-frequency = <1600000>;
					bias-pull-down;
				};

				s3 {
					regulator-min-microvolt = <500000>;
					regulator-max-microvolt = <1150000>;
					qcom,switch-mode-frequency = <4800000>;
					bias-pull-down;
				};

				s4 {
					regulator-always-on;
					regulator-min-microvolt	= <1800000>;
					regulator-max-microvolt	= <1800000>;
					qcom,switch-mode-frequency = <1600000>;
					bias-pull-down;
				};

				s7 {
					regulator-min-microvolt = <1300000>;
					regulator-max-microvolt = <1300000>;
					qcom,switch-mode-frequency = <3200000>;
				};

				s8 {
					regulator-min-microvolt = <2200000>;
					regulator-max-microvolt = <2200000>;
					qcom,switch-mode-frequency = <1600000>;
					bias-pull-down;
				};

				/* LDO */
				l3 {
					regulator-min-microvolt = <3075000>;
					regulator-max-microvolt = <3075000>;
					bias-pull-down;
				};

				l4 {
					regulator-always-on;
					regulator-min-microvolt = <1800000>;
					regulator-max-microvolt = <1800000>;
					bias-pull-down;
				};

				l5 {
					regulator-min-microvolt = <2950000>;
					regulator-max-microvolt = <2950000>;
					bias-pull-down;
				};

				l6 {
					regulator-min-microvolt = <2950000>;
					regulator-max-microvolt = <2950000>;
					bias-pull-down;
				};

				l7 {
					regulator-min-microvolt = <1850000>;
					regulator-max-microvolt = <2950000>;
					bias-pull-down;
				};

				l8 {
					regulator-always-on;
					regulator-min-microvolt = <1800000>;
					regulator-max-microvolt = <1800000>;
					bias-pull-down;
				};

				l9 {
					regulator-min-microvolt = <3000000>;
					regulator-max-microvolt = <3000000>;
					bias-pull-down;
				};

				l10 {
					regulator-min-microvolt = <2900000>;
					regulator-max-microvolt = <2900000>;
					bias-pull-down;
				};

				l11 {
					regulator-min-microvolt = <3000000>;
					regulator-max-microvolt = <3000000>;
					bias-pull-down;
				};

				l13 {
					regulator-min-microvolt = <2220000>;
					regulator-max-microvolt = <2220000>;
				};

				l14 {
					regulator-min-microvolt = <1800000>;
					regulator-max-microvolt = <1800000>;
					bias-pull-down;
				};

				l15 {
					regulator-always-on;
					regulator-min-microvolt = <2500000>;
					regulator-max-microvolt = <2500000>;
					bias-pull-down;
				};

				l16 {
					regulator-min-microvolt = <2800000>;
					regulator-max-microvolt = <2800000>;
					bias-pull-down;
				};

				l17 {
					regulator-min-microvolt = <2000000>;
					regulator-max-microvolt = <2000000>;
					bias-pull-down;
				};

				l21 {
					regulator-min-microvolt = <1050000>;
					regulator-max-microvolt = <1050000>;
					bias-pull-down;
				};

				l22 {
					regulator-min-microvolt = <2600000>;
					regulator-max-microvolt = <2600000>;
					bias-pull-down;
				};

				l23 {
					regulator-min-microvolt = <1800000>;
					regulator-max-microvolt = <1800000>;
					bias-pull-down;
				};

				l29 {
					regulator-min-microvolt = <2000000>;
					regulator-max-microvolt = <2000000>;
					bias-pull-down;
				};

				/* VS */

				lvs1 {
					bias-pull-down;
				};

				lvs3 {
					bias-pull-down;
				};

				lvs4 {
					bias-pull-down;
				};

				lvs5 {
					bias-pull-down;
				};

				lvs6 {
					bias-pull-down;
				};
			};
		};

                gsbi@16300000 {
                        status = "ok";
                        qcom,mode = <GSBI_PROT_I2C_UART>;
                        serial@16340000 {
                                status = "ok";
                        };
                };

                usb@12500000 {
                        status = "okay";
                        dr_mode = "host";
                        ulpi {
                                phy {
                                        v3p3-supply = <&pm8921_l3>;
                                        v1p8-supply = <&pm8921_l4>;
                                };
                        };
                };

                usb@12520000 {
                        status = "okay";
                        dr_mode = "host";
                        ulpi {
                                phy {
                                        v3p3-supply = <&pm8921_l3>;
                                        v1p8-supply = <&pm8921_l23>;
                                };
                        };
                };

                usb@12530000 {
                        status = "okay";
                        dr_mode = "host";
                        ulpi {
                                phy {
                                        v3p3-supply = <&pm8921_l3>;
                                        v1p8-supply = <&pm8921_l23>;
                                };
			};
		};

                amba {
                        /* eMMC */
                        sdcc1: sdcc@12400000 {
                                status = "okay";
                                vmmc-supply = <&pm8921_l5>;
                                vqmmc-supply = <&pm8921_s4>;
                        };
		};

                hdmi-tx@4a00000 {
                        status = "okay";

                        core-vdda-supply = <&pm8921_hdmi_switch>;

                        ports {
                                port@0 {
                                        hdmi0_in: endpoint {
                                                remote-endpoint = <&mdp_hdmi0_out>;
                                        };
                                };

                                port@1 {
                                        hdmi0_out: endpoint {
                                                remote-endpoint = <&hdmi0_con>;
                                        };
                                };
                        };
                };

                hdmi-phy@4a00400 {
                        status = "okay";

                        core-vdda-supply = <&pm8921_hdmi_switch>;
                };

                mdp@5100000 {
                        status = "okay";

                        ports {
                                port@3 {
                                        mdp_hdmi0_out: endpoint {
                                                remote-endpoint = <&hdmi0_in>;
                                        };
                                };
                        };
                };
	};
};
